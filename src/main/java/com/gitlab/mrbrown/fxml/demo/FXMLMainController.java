package com.gitlab.mrbrown.fxml.demo;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import static com.gitlab.mrbrown.fxml.demo.App.VIEW_MODEL;

public class FXMLMainController {

    // Ersetzen mit DI
    private ViewModel viewModel = VIEW_MODEL;

    @FXML
    public void onMainButtonAction(ActionEvent actionEvent) {
        VIEW_MODEL.incrementLabel1Rotation();
    }

    public ViewModel getViewModel() {
        return viewModel;
    }
}
