package com.gitlab.mrbrown.fxml.demo;

import javafx.beans.property.*;

public class ViewModel {

    DoubleProperty mainButtonRotation = new SimpleDoubleProperty();

    DoubleProperty label1Rotation = new SimpleDoubleProperty();

    DoubleProperty text1Rotation = new SimpleDoubleProperty();

    BooleanProperty textFieldEditable = new SimpleBooleanProperty(true);

    public void incrementMainButtonRotation() {
        this.mainButtonRotation.set(mainButtonRotation.get()+20);
    }

    public DoubleProperty mainButtonRotationProperty() {
        return mainButtonRotation;
    }


    public void incrementLabel1Rotation() {
        this.label1Rotation.set(label1Rotation.get()+20);
    }

    public void incrementText1Rotation() {
        this.text1Rotation.set(text1Rotation.get()+20);
    }

    public DoubleProperty label1RotationProperty() {
        return label1Rotation;
    }

    public DoubleProperty text1RotationProperty() {
        return text1Rotation;
    }

    public double getMainButtonRotation() {
        return mainButtonRotation.get();
    }

    public double getLabel1Rotation() {
        return label1Rotation.get();
    }

    public double getText1Rotation() {
        return text1Rotation.get();
    }

    public boolean getTextFieldEditable() {
        return textFieldEditable.get();
    }

    public BooleanProperty textFieldEditableProperty() {
        return textFieldEditable;
    }
}
