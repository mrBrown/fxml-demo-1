package com.gitlab.mrbrown.fxml.demo;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    /**
     * Statt static Dependency-Injection nutzen, hier nur der Einfachheit halber so genutzt.
     * <p>
     * zb Guice, Spring & CDI lassen sich mit JavaFX nutzen.
     */
    public static final ViewModel VIEW_MODEL = new ViewModel();

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/FXMLMain.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Hello World Example");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String... args) {
        launch(args);
    }

}
