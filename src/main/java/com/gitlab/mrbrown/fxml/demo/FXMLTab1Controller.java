package com.gitlab.mrbrown.fxml.demo;

import javafx.fxml.FXML;

import static com.gitlab.mrbrown.fxml.demo.App.VIEW_MODEL;

public class FXMLTab1Controller {

    // Ersetzen mit DI
    private ViewModel viewModel = VIEW_MODEL;

    @FXML
    public void onB1Action() {
        VIEW_MODEL.incrementMainButtonRotation();
    }

    public ViewModel getViewModel() {
        return viewModel;
    }
}
