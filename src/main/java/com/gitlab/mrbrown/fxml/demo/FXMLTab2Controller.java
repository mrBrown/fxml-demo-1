package com.gitlab.mrbrown.fxml.demo;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;

import static com.gitlab.mrbrown.fxml.demo.App.VIEW_MODEL;

public class FXMLTab2Controller {

    // Ersetzen mit DI
    private ViewModel viewModel = VIEW_MODEL;

    @FXML
    private CheckBox checkbox;

    public void initialize() {
        checkbox.selectedProperty().bindBidirectional(viewModel.textFieldEditableProperty());
    }

    @FXML
    public void onB2Action() {
        VIEW_MODEL.incrementText1Rotation();
    }

}
