module com.gitlab.mrbrown.fxml.demo {
    requires javafx.controls;
    requires javafx.fxml;
    exports com.gitlab.mrbrown.fxml.demo to javafx.graphics;
    opens com.gitlab.mrbrown.fxml.demo to javafx.fxml;
}
